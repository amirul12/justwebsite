<?php include('config.php');?>
<!DOCTYPE HTML>
<html>
<head>
        <meta charset="utf-8">
        
        <title>JUST| Jessore</title> 
        <!----------------------------------------------************ Stylesheet ************** --------------------------------------->
        <link rel="stylesheet" type="text/css" href="public/css/font-awesome.css"/>
        <link rel="stylesheet" type="text/css" href="public/style.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/custom.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/responsive.css"/>
        <!----------------------------------------------************ Stylesheet closed ************** --------------------------------------->

        <!----------------------------------------------************ Jquery & Plugins Started ************** -------------------------------->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"> </script>-->
        <script src="public/js/jQuery.js"></script>

        <!----------------------------------------------************ Jquery & Plugins Closed ************** -------------------------------->

        <script src="public/js/nav.js" type="text/javascript"></script>

        <!----------------------------------------------************ Local Script closed ************** ------------------------------------------->
        <!-- Skitter Styles -->
        <link href="public/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />

        <!-- Skitter JS -->
        <script type="text/javascript" language="javascript" src="public/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" language="javascript" src="public/js/jquery.skitter.min.js"></script>

        <!-- FFocus Slider -->


        <link rel="stylesheet" href="public/css/QFocus.css" />
        <!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
        <script src="public/js/QFocus.js"></script>

        <script>
            $(function () {
                $("#div1").QFocus();
                $("#div2").QFocus({navControl: false});
                $("#div3").QFocus({type: "fade"});
            })
        </script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>


        <!-- Init Skitter -->
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $('.box_skitter_large').skitter({
                    theme: 'clean',
                    numbers_align: 'center',
                    progressbar: false,
                    dots: false,
                    preview: false
                });
            });
        </script>
    </head>

    <body>
        <div class="wrapper">
            <header class="topbar"> 
                <img src="public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->

		<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                <ul class="mainmenu">
                    <li><a href="home.php">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="page/index/just_history.php">JUST History</a> </li>
                                <li> <a href="page/index/future_plan.php">Future Plan</a> </li>
                                <li> <a href="page/index/just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>
                    </li>
                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/chancellor.php">Chancellor</a></li>
                                <li> <a href="page/index/vice_chancellor.php">Vice-Chancellor</a></li>
								<li> <a href="page/index/university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="page/index/administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="page/index/degree_offered.php">Degree Offered</a></li>
								<li> <a href="page/index/admission.php">Admission</a></li>
                                <li> <a href="page/index/academic_council.php">Academic Council</a></li>
                                <li> <a href="page/index/academic_expenses.php">Academic Expenses</a></li>                     
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/bncc.php">BNCC</a> </li>
                                <li> <a href="page/index/rover_scout.php">Rover Scout</a></li>
                                <li> <a href="page/index/sports.php">Sports</a></li>
                                <li> <a href="page/index/cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="page/index/library.php">Library</a></li>
                                <li> <a href="page/index/academic_facilities.php">Accomodetion</a></li>
                                <li> <a href="page/index/scholarship.php">Scholarship</a></li>
                                <li> <a href="page/index/study_tour.php">Transport</a></li>
								<li> <a href="page/index/medical.php">Medical</a></li>					
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="#">Faculty Info</a>
					    <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/et.php">Engineering and Technology</a> </li>
                                <li> <a href="page/index/bst.php">Biological Science and Technology</a></li>
								<li> <a href="page/index/ast.php">Applied Science and Technology</a></li>
                                <li> <a href="page/index/peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="page/index/foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                    <li><a href="home/search/student.php">Students</a></li>
                    <li><a href="home/notice/notice.php">Notice</a></li>
                    <li><a href="home/overview/gallery.php">Gallery</a></li>
                    <li> <a href="home/contact.php">Contact</a></li>
                    <li><a href="access/signIn.php">Login</a></li>

                </ul>
            </nav>
 
 <!------------------------------------ Banner Area Closed --------------------------------------->

            <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" 
				onMouseOver="document.getElementById('news').stop();" 
				onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>
            </div>

            <div class="banner-main">
                <div class="banner1">
                    <div id="div1" class="QFocus">
                        <ul>
                            <li><a href='#'><img src='public/slider/1.jpg' title='AcademyBuilding' /></a></li>
							<li><a href='#'><img src='public/slider/2.jpg' title='Dormetory' /></a></li> 
							<li><a href='#'><img src='public/slider/3.jpg' title='AdministrativeBuilding' /></a></li>
							<li><a href='#'><img src='public/slider/4.jpg' title='Womens Hall' /></a></li> 							
						</ul>
                    </div>
                </div>
                
                <div class="banner2">
                    <div class="noticeboard-top social">
                        <h3>Departmental Notice</h3>
                        <marquee id="d_news" behavior="scroll" direction="up" scrollamount="3" onMouseOver="document.getElementById('d_news').stop();" 
							onMouseOut="document.getElementById('d_news').start();">
                            <p><a href="#">Dept. Of CSE</a></p> 
							<p><a href="#">Dept. Of EEE</a></p> 
							<p><a href="#">Dept. Of PME</a></p> 
							<p><a href="#">Dept. Of IPE</a></p> 
							<p><a href="#">Dept. Of ChE</a></p> 
							<p><a href="#">Dept. Of MB</a></p> 
							<p><a href="#">Dept. Of FMB</a></p>							

						</marquee>
                    </div>
                    
                </div>
            </div>
            <!------------------------------------ Banner Area Closed --------------------------------------->

          <script type="text/javascript">
                $(document).ready(function () {
                    // mouseenter + mouseout in marquee tag
                    $('#news').on({
                        'mouseenter': function () {
                            $(this).closest('marquee').attr('scrollamount', 0);
                        },
                        'mouseout': function () {
                            $(this).closest('marquee').attr('scrollamount', 3);
                        }
                    });
                });
            </script>
			 <script type="text/javascript">
                $(document).ready(function () {
                    // mouseenter + mouseout in marquee tag
                    $('#d_news').on({
                        'mouseenter': function () {
                            $(this).closest('marquee').attr('scrollamount', 0);
                        },
                        'mouseout': function () {
                            $(this).closest('marquee').attr('scrollamount', 3);
                        }
                    });
                });
            </script>
            
            <script type="text/javascript">
        $(document).ready(function() {
            // mouseenter + mouseout in marquee tag
            $('.noticeboard-top marquee p a').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script>
<style>
.maincontent{}
</style>

<!--[
    - c: home/index
    - p: home
    - v: $title, $admin, $honour, $master, $hsc, $view
]-->

<article class="maincontent" style="margin-top:0px;">  
   
       <section class="sammary row">
   
		<h2 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc; color:#0164FA;">Welcome to JUST</h2>
		<p style="padding: 10px; text-align: justify; line-height: 25px;font-size:15px;">    
	    Jessore University of Science and Technology (JUST) is a newly established renowned public university of Bangladesh. It was established in 2007. In 2010, Hon'ble Prime Minister Sheikh Hasina formally inaugurated the new campus of JUST.
	</section>
	
	<section class="sammary row">
       <img src="public/img/vc.jpg"alt="" width="110" style="margin:3px 10px 5px 3px;float:left;"/>
        <h3 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc;color:#0164FA;">Message from Vice Chancellor..</h3>
	<p style="padding: 10px; text-align: justify; line-height: 25px;font-size:15px;">
							It is my great pleasure to invite you to explore the Jessore University of Science and
							Technology (JUST) online through our website. Contribution of science and technology for 
							developing a nation is well known to all. To meet up the diversified demand of people, 
							information and communication technology,and biological sciences are playing the key role. 
							University is the most suitable place for education and research. Universities are playing a vital
							role in building efficient manpower for the development of the country as well as for the global 
							need. With a view to imparting science and technology oriented education in Bangladesh, the Jessore
							University of Science & Technology was established in 2007 by the Shadhinota Shorok (Independence 
							Road) in Jessore district. The first batch of students was admitted in 2008-2009 session. The 
							honorable Prime Minister of the Peoples Republic of Bangladesh, Sheikh Hasina, inaugurated the 
							campus of the university on December 27, 2010.Within a very short period, the JUST already 
							demonstrated its outreach excellence through establishing linkages with the various government 
							and non-government organizations for research, extension and developmental activities. Our 
							honorable Prime Minister has already declared the Vision 2021 for the digitization (advance 
							technology based, economically developed, corruption free) of the country, Bangladesh. I strongly
							believe that this university will play a significant role to fulfil this vision by producing 
							science and technology based efficient manpower and enlightened citizen. We always adapt the 
							positive changes which is the need of time. Thus, by using this science and technological 
							knowledge we are motivated to boost up the world's technology. As the Vice Chancellor of the 
							Jessore University of Science and Technology, I congratulate all and wish its success in future.
	</section>
	
			
    <section class="sammary row">
         <h2 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc;color:#0164FA;">News and Events</h2>			
			<div class="newsandevents social">
			<?php
			$statement = $db->prepare("SELECT * FROM tbl_cnotice_post");
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($result as $row)
			{
			?>
			<ul>
				<li><a href="index2.php?id=<?php echo $row['cnotice_id'];?>"> <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/><?php echo $row['cnotice_title'];?></a></li>
			</ul>
				
			<?php
			}
			
			?>
			 
			</div>
    </section>
    

    <script type="text/javascript">
        $(document).ready(function() {
            // mouseenter + mouseout in marquee tag
            $('.noticeboard marquee p a').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script>
</article>




<aside class="right-panel">
<div class="singlediv">
    <!-- <h1 class="title">Total view 0</h1>-->
        <h1 class="title"> Central Notice</h1>
        <ul>
            <li> <a href="page/index/just_history.php"> History</a></li>
            <li> <a href="page/index/future_plan.php">Future Plan</a></li>
            <li> <a href="page/index/campus.php">Campus</a></li>
            <li> <a href="page/index/academic_facilities.php">Academic Facilities</a></li>
            <li> <a href="page/index/just_infrastructure.php"> JUST Infrastructure</a></li>
			<li> <a href="page/index/archive.php"> Archive</a></li>

        </ul>

    </div>


    <div class="singlediv">
        <h1 class="title"> Download </h1>
        <ul>
            <li> <a href="home/overview/result.php">Result</a></li>
            <li> <a href="#">Class Routine</a></li>
            <li> <a href="#">Academic Calender</a></li>
        </ul>
    </div>
	    <div class="singlediv">
        <h1 class="title">JUST WebMail</h1>
        <ul>
            <li><a href="http://webmail.just.edu.bd:2095">
		        <p align="center"><img  src="home/img/webmail.jpg" width="200" height="120"></p></a>
			</li>
        </ul>
    </div>
	
    <div class="singlediv">
        <h1 class="title">Quick Links</h1>
        <ul>
            <li> <a href="http://www.moedu.gov.bd/" target="_blank"> MOEDU  </a></li>
            <li> <a href="http://www.dshe.gov.bd/" target="_blank">DSHE </a></li>
            <li> <a href="http://www.shikkhok.com/" target="_blank">Shikkhok.com</a></li>
            <li> <a href="http://www.bangladesh.gov.bd/" target="_blank">National Web Portal</a></li>
            <li> <a href="http://www.Jessore.gov.bd/" target="_blank">Jessore Zilla</a></li>
        </ul>
    </div>
	
	<div class="singlediv">
        <h1 class="title">Social Link</h1>
		<ul>
		 <li><a href="www.facebook.com">
		 <img  src="public/img/f.png" width="35" height="35" align= "middle"/>
		   Facebook</a></li>
		 		 <li><a href="www.twitter.com">
		 <img src="public/img/t.png" width="35" height="35" align= "middle" />
		   Twitter</a></li>
		 		 <li><a href="www.Feed.com">
		 <img  src="public/img/r.png" width="35" height="35" align= "middle" />
		  Feed</a></li>
		  		 <li><a href="www.Linkedin.com">
		 <img  src="public/img/l.png" width="35" height="35" align= "middle" />
		  Linkedin</a></li>
		</ul>
	</div>

</aside>



</div> <!------------------------------------- wrapper div closed --------------------------------------->


<footer class="foot-1">
    <section class="wrapper">
        <div>
            <ul class="foot-ul-1">
                <li><a href="home/search/student.php"><i class="fa fa-caret-right"></i>Students</a></li>
                <li><a href="home/teachers.php"><i class="fa fa-caret-right"></i>Teachers</a></li>
                <li><a href="home/staffs.php"><i class="fa fa-caret-right"></i>Staff</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Committee</a></li>
				<li><a href="home/overview/result.php"><i class="fa fa-caret-right"></i>Result</a></li>
				<li><a href="page/index/archive.php"><i class="fa fa-caret-right"></i>Archive</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-2">
                <li><a href="#"><i class="fa fa-caret-right"></i>Academic Calender</a></li>
                <li><a href="home/overview/digital_content.php"><i class="fa fa-caret-right"></i>Digital Content</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Class Routine</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Dept. Order</a></li>
                <li><a href="page/index/admission.php"><i class="fa fa-caret-right"></i>Admission</a></li>
                <li><a href="home/overview/gallery.php"><i class="fa fa-caret-right"></i>Gallery</a></li>
            </ul>
        </div>

        <div class="foot-logo">
            <ul class="foot-ul-1">
                <li><a href="page/index/bncc.php"><i class="fa fa-caret-right"></i>BNCC</a></li>
                <li><a href="page/index/campus.php"><i class="fa fa-caret-right"></i>Campus</a></li>
                <li><a href="page/index/rover_scout.php"><i class="fa fa-caret-right"></i>Rover Scout</a></li>
                <li><a href="page/index/academic_facilities.php"><i class="fa fa-caret-right"></i>Academic Facilities</a></li>
                <li><a href="page/index/just_infrastructure.php"><i class="fa fa-caret-right"></i>JUST Infrastructure</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-3">
                <li><a href="page/index/just_achievement.php"><i class="fa fa-caret-right"></i>JUST Achievement</a></li>
                <li><a href="page/index/just_history.php"><i class="fa fa-caret-right"></i>JUST History</a></li>
                <li><a href="page/index/future_plan.php"><i class="fa fa-caret-right"></i>Future Plan</a></li>
                <li><a href="home/contact.php"><i class="fa fa-caret-right"></i>Contact Us</a></li>
                <li><a href="page/index/admission.php"><i class="fa fa-caret-right"></i>Scholarship</a></li>
                <li><a href="page/index/study_tour.php"><i class="fa fa-caret-right"></i>Study Tour</a></li>
            </ul>
        </div>

        
        <div>
            <ul class="foot-ul-4">
                <li>Jessore University Of Science & Technology, Jessore.</li>
                <li>Phone: 091 - *****</li>
                <li>Mobile: 091 - *****</li>
                <li>Fax: 091 - *****</li>
            </ul>
        </div>
    </section>
</footer>

<section class="foot-2">
 
<div class="copyright">
	<p id="copytxt" style="margin-left:-30px !important;">&copy; Jessore University of Science & Technology</p>
</div>

	
	<div class="social">
       <!--
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
       
        <a href="#"><i class="fa fa-youtube-play"></i></a>
        <a href="#"><i class="fa fa-skype"></i></a>
        <a href="#"><i class="fa fa-yahoo"></i></a>
        -->
    </div>
    
   <div class="developer">
	<p id="developtxt">Developed by : <a href="#">Dept. of CSE</a>, JUST</p>
	</div>
</section>
	
	<!--digital clock start-->
	<script type= "text/javascript">
		$(document).ready(function(){
			var contant = $('#calendar');
			
			setInterval(function(){
				var currentdate = new Date(); 
				var datetime = "<p>" + currentdate.getDate() + "/"
				+ (currentdate.getMonth()+1)  + "/" 
				+ currentdate.getFullYear() + "</p><p> "  
				+ currentdate.getHours() + ":"  
				+ currentdate.getMinutes() + ":" 
				+ currentdate.getSeconds() + "</p>";
				
				contant.php(datetime);
			}, 1000);
		});
	</script>
	<!--digital clock end-->
</body>
</html>

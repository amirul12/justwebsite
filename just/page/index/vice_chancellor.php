<?php include("header.php") ?>

    <body>
        <div class="wrapper">
            <header class="topbar"> 
               <img src="../../public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->


           				<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                  <ul class="mainmenu">
                    <li><a href="../../home.php">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="just_history.php">JUST History</a> </li>
                                <li> <a href="future_plan.php">Future Plan</a> </li>
                                <li> <a href="just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>

                    </li>

                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="chancellor.php">Chancellor</a></li>
                                <li> <a href="vice_chancellor.php">Vice-Chancellor</a></li>
								<li> <a href="university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                              <ul>
                                <li> <a href="academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="degree_offered.php">Degree Offered</a></li>
								<li> <a href="admission.php">Admission</a></li>
                                <li> <a href="academic_council.php">Academic Council</a></li>
                                <li> <a href="academic_expenses.php">Academic Expenses</a></li>                     
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="bncc.php">BNCC</a> </li>
                                <li> <a href="rover_scout.php">Rover Scout</a></li>
                                <li> <a href="sports.php">Sports</a></li>
                                <li> <a href="cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="library.php">Library</a></li>
                                <li> <a href="accomodetion.php">Accomodetion</a></li>
                                <li> <a href="scholarship.php">Scholarship</a></li>
                                <li> <a href="transport.php">Transport</a></li>
								<li> <a href="medical.php">Medical</a></li>					
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="home/teachers.php">Faculty Info</a>
					    <div id="soho-karzokrom">
                              <ul>
                                <li> <a href="et.php">Engineering and Technology</a> </li>
                                <li> <a href="bst.php">Biological Science and Technology</a></li>
								<li> <a href="ast.php">Applied Science and Technology</a></li>
                                <li> <a href="peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                        <li><a href="../../home/search/student.php">Students</a></li>
                    <li><a href="../../home/notice/notice.php">Notice</a></li>
                    <li><a href="../../home/overview/gallery.php">Gallery</a></li>
                    <li> <a href="../../home/contact.php">Contact</a></li>
                    <li><a href="../../access/signIn.html">Login</a></li>

                </ul>
            </nav>
 
 <!------------------------------------ Banner Area Closed --------------------------------------->

            <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" onMouseOver="document.getElementById('news').stop();" onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>
            </div>
            
       <script type="text/javascript">
        $(document).ready(function() {
            
            
            
            // mouseenter + mouseout in marquee tag
            $('#news').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script><!--[
    - c: page/index
    - p: page
    - v: 
]-->

<article class="maincontent">
		<h1>Message from VC.......</h1><br>
				 <center><img src="img/vc.jpg" border="2px solid "></center><br>
					<div class="center-justified">
					<p style="text-align:justify">It is my great pleasure to invite you to explore the Jessore University of Science and
							Technology (JUST) online through our website. Contribution of science and technology for 
							developing a nation is well known to all. To meet up the diversified demand of people, 
							information and communication technology,and biological sciences are playing the key role. 
							University is the most suitable place for education and research. Universities are playing a vital
							role in building efficient manpower for the development of the country as well as for the global 
							need. With a view to imparting science and technology oriented education in Bangladesh, the Jessore
							University of Science & Technology was established in 2007 by the Shadhinota Shorok (Independence 
							Road) in Jessore district. The first batch of students was admitted in 2008-2009 session. The 
							honorable Prime Minister of the Peoples Republic of Bangladesh, Sheikh Hasina, inaugurated the 
							campus of the university on December 27, 2010.</p><br><p style="text-align: justify">Within a very short period, the JUST already 
							demonstrated its outreach excellence through establishing linkages with the various government 
							and non-government organizations for research, extension and developmental activities. Our 
							honorable Prime Minister has already declared the Vision 2021 for the digitization (advance 
							technology based, economically developed, corruption free) of the country, Bangladesh. I strongly
							believe that this university will play a significant role to fulfil this vision by producing 
							science and technology based efficient manpower and enlightened citizen. We always adapt the 
							positive changes which is the need of time. Thus, by using this science and technological 
							knowledge we are motivated to boost up the world's technology. As the Vice Chancellor of the 
							Jessore University of Science and Technology, I congratulate all and wish its success in future.</p>				
					<br>
				 <h3><i><b>Professor Dr. Md. Abdus Sattar</b></i></h3><br>
			     <p>Vice Chancellor</p>
				 <p>Jessore University of Science and Technology</p>
				 <p>Jessore, Bangladesh</p><br>
					
			</div>
    
    <section class="row">
            </section>
</article>

 <?php include("rightside.php") ?>



</div> <!------------------------------------- wrapper div closed --------------------------------------->

 <?php include("footer.php") ?>
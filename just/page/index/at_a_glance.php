<?php include("header.php") ?>
    <body>
        <div class="wrapper">
            <header class="topbar"> 
               <img src="../../public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->


           				<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

 <ul class="mainmenu">
                       <li><a href="../../home.php">Home</a></li>
                       <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="just_history.php">JUST History</a> </li>
                                <li> <a href="future_plan.php">Future Plan</a> </li>
                                <li> <a href="just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>

                    </li>

                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="chancellor.php">Chancellor</a></li>
                                <li> <a href="vice_chancellor.php">Vice-Chancellor</a></li>
								<li> <a href="university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                          <ul>
                                <li> <a href="academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="degree_offered.php">Degree Offered</a></li>
								<li> <a href="admission.php">Admission</a></li>
                                <li> <a href="academic_council.php">Academic Council</a></li>
                                <li> <a href="academic_expenses.php">Academic Expenses</a></li>                     
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="bncc.php">BNCC</a> </li>
                                <li> <a href="rover_scout.php">Rover Scout</a></li>
                                <li> <a href="sports.php">Sports</a></li>
                                <li> <a href="cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                             <ul>
								<li> <a href="library.php">Library</a></li>
                                <li> <a href="accomodetion.php">Accomodetion</a></li>
                                <li> <a href="scholarship.php">Scholarship</a></li>
                                <li> <a href="transport.php">Transport</a></li>
								<li> <a href="medical.php">Medical</a></li>					
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="home/teachers.php">Faculty Info</a>
					    <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="et.php">Engineering and Technology</a> </li>
                                <li> <a href="bst.php">Biological Science and Technology</a></li>
								<li> <a href="ast.php">Applied Science and Technology</a></li>
                                <li> <a href="peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                        <li><a href="../../home/search/student.php">Students</a></li>
                    <li><a href="../../home/notice/notice.php">Notice</a></li>
                    <li><a href="../../home/overview/gallery.php">Gallery</a></li>
                    <li> <a href="../../home/contact.php">Contact</a></li>
                    <li><a href="../../access/signIn.html">Login</a></li>

                </ul>


            </nav><!------------------------------------ Banner Area Started--------------------------------------->
            
            <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" onMouseOver="document.getElementById('news').stop();" onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>
            </div>
            
       <script type="text/javascript">
        $(document).ready(function() {
            
            
            
            // mouseenter + mouseout in marquee tag
            $('#news').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script><!--[
    - c: page/index
    - p: page
    - v: 
]-->

<article class="maincontent">
    <h1>At a glance</h1>
     
    <section class="row">
		<center><img src="img/g.png" border=" 2px solid #0264FA"/></center></br>
				<u><h3>STUDENT COUNSELING & GUIDANCE</h3></u>
				<p>Office of the Student Counseling & Guidance provides a range of support services to students towards their 
				personal development. As a result the students can utilize their knowledge and skills towards the national 
				development. Services provided by this office address academic, personal/social, and career and individual 
				development. Many student clubs, organizations are directed by this office so that the students can involve 
				different extracurricular activities.</p></br>
				<h3>Contact</h3>
				<p>Director</p>
				<h4>Dr. Md. Mer Mosharraf Hossain</h4>
				<p>Assistant Professor</p>
				<p>Department of Fisheries and Marine Bioscience</p>
				<p>Jessore University of Science and Technology </p></br>
				<u><h3>MEDICAL SERVICES</h3></u>
				<p>There is a medical center in JUST campus for the students, faculty and staff. To face the emergency 
				transportation the center has its own medical ambulance service.</p></br>
				<u><h3>CENTRAL CAFETERIA</u></h3>
				<center><img src="img/cafe.jpg" border=" 2px solid #0264FA"/></center></br>
				<p>JUST Cafeteria is located beside the main entrance of the campus. Foods are available here for the students,
				faculty and staff of JUST. Apart from the common snacks and drinks; breakfast and lunch can also be found here.
				Anyone may get rice, pulse, vegetables, eggs of chicken/duck, fish, chicken, mutton at an affordable price.</p></br>
				<u><h3>AUDITORIUM</h3></u></br>
				<p>A beautiful auditorium/gallery is located at the ground floor of the academic building. Special events like 
				seminars, conferences, cultural programs, discussion, meetings etc. generally take place here.</p></br>
				<u><h3>TRANSPORT FACILITIES</h3></u></br>
				<p>JUST operates shuttle services for students, faculty and staff from campus to different destinations of 
				Jessore City. The University has its own transportation facilities. The common shuttle service destinations 
				are Palbari, Arobpur, Dharmotola, Chacra, New-market, and Jessore City Center (Daratana).</p></br>
				<center><img src="img/bus.png" border=" 2px solid #0264FA"/></center></br>
				<u><h3>INTERNET</h3></u></br>
				<p>Internet is an important part of the University experience to ensure the students stay connected to the world.
				Generally, information is distributed via internet and social communications are routinely made by email. 
				Students can get internet services from the university computer center & university WiFi.</p></br> 
				<center><img src="img/Ccenter.jpg" border=" 2px solid #0264FA"/></center></br>
           </section>
</article>

 <?php include("rightside.php") ?>



</div> <!------------------------------------- wrapper div closed --------------------------------------->

 <?php include("footer.php") ?>
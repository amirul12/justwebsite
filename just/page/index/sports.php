<?php include("header.php") ?>

    <body>
        <div class="wrapper">
            <header class="topbar"> 
               <img src="../../public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->


           				<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                <ul class="mainmenu">
                   <li><a href="../../home.php">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="just_history.php">JUST History</a> </li>
                                <li> <a href="future_plan.php">Future Plan</a> </li>
                                <li> <a href="just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>

                    </li>

                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                           <ul>
                                <li> <a href="chancellor.php">Chancellor</a></li>
                                <li> <a href="vice_chancellor.php">Vice-Chancellor</a></li>
								<li> <a href="university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                              <ul>
                                <li> <a href="academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="degree_offered.php">Degree Offered</a></li>
								<li> <a href="admission.php">Admission</a></li>
                                <li> <a href="academic_council.php">Academic Council</a></li>
                                <li> <a href="academic_expenses.php">Academic Expenses</a></li>                     
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="bncc.php">BNCC</a> </li>
                                <li> <a href="rover_scout.php">Rover Scout</a></li>
                                <li> <a href="sports.php">Sports</a></li>
                                <li> <a href="cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="library.php">Library</a></li>
                                <li> <a href="accomodetion.php">Accomodetion</a></li>
                                <li> <a href="scholarship.php">Scholarship</a></li>
                                <li> <a href="transport.php">Transport</a></li>
								<li> <a href="medical.php">Medical</a></li>					
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="home/teachers.php">Faculty Info</a>
					    <div id="soho-karzokrom">
                             <ul>
                                <li> <a href="et.php">Engineering and Technology</a> </li>
                                <li> <a href="bst.php">Biological Science and Technology</a></li>
								<li> <a href="ast.php">Applied Science and Technology</a></li>
                                <li> <a href="peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                        <li><a href="../../home/search/student.php">Students</a></li>
                    <li><a href="../../home/notice/notice.php">Notice</a></li>
                    <li><a href="../../home/overview/gallery.php">Gallery</a></li>
                    <li> <a href="../../home/contact.php">Contact</a></li>
                    <li><a href="../../access/signIn.html">Login</a></li>

                </ul>
            </nav>
 
 <!------------------------------------ Banner Area Closed --------------------------------------->

            <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" onMouseOver="document.getElementById('news').stop();" onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>
            </div>

            
       <script type="text/javascript">
        $(document).ready(function() {
            
            
            
            // mouseenter + mouseout in marquee tag
            $('#news').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script><!--[
    - c: page/index
    - p: page
    - v: 
]-->

<article class="maincontent">
    <h1>Sports</h1>
    
    <section class="row">
            </section>
</article>

 <?php include("rightside.php") ?>


</div> <!------------------------------------- wrapper div closed --------------------------------------->


 <?php include("footer.php") ?>
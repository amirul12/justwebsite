
<footer class="foot-1">
    <section class="wrapper">
        <div>
            <ul class="foot-ul-1">
                <li><a href="file:///C|/wamp/www/home/search/student.php"><i class="fa fa-caret-right"></i>Students</a></li>
                <li><a href="file:///C|/wamp/www/home/teachers.php"><i class="fa fa-caret-right"></i>Teachers</a></li>
                <li><a href="file:///C|/wamp/www/home/staffs.php"><i class="fa fa-caret-right"></i>Staff</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Committee</a></li>
				<li><a href="file:///C|/wamp/www/home/overview/result.php"><i class="fa fa-caret-right"></i>Result</a></li>
				<li><a href="file:///C|/wamp/www/page/index/archive.php"><i class="fa fa-caret-right"></i>Archive</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-2">
                <li><a href="#"><i class="fa fa-caret-right"></i>Academic Calender</a></li>
                <li><a href="file:///C|/wamp/www/home/overview/digital_content.php"><i class="fa fa-caret-right"></i>Digital Content</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Class Routine</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Dept. Order</a></li>
                <li><a href="file:///C|/wamp/www/page/index/admission.php"><i class="fa fa-caret-right"></i>Admission</a></li>
                <li><a href="file:///C|/wamp/www/home/overview/gallery.php"><i class="fa fa-caret-right"></i>Gallery</a></li>
            </ul>
        </div>

        <div class="foot-logo">
            <ul class="foot-ul-1">
                <li><a href="file:///C|/wamp/www/page/index/bncc.php"><i class="fa fa-caret-right"></i>BNCC</a></li>
                <li><a href="file:///C|/wamp/www/page/index/campus.php"><i class="fa fa-caret-right"></i>Campus</a></li>
                <li><a href="file:///C|/wamp/www/page/index/rover_scout.php"><i class="fa fa-caret-right"></i>Rover Scout</a></li>
                <li><a href="file:///C|/wamp/www/page/index/academic_facilities.php"><i class="fa fa-caret-right"></i>Academic Facilities</a></li>
                <li><a href="file:///C|/wamp/www/page/index/just_infrastructure.php"><i class="fa fa-caret-right"></i>JUST Infrastructure</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-3">
                <li><a href="just_achievement.php"><i class="fa fa-caret-right"></i>JUST Achievement</a></li>
                <li><a href="file:///C|/wamp/www/page/index/just_history.php"><i class="fa fa-caret-right"></i>JUST History</a></li>
                <li><a href="file:///C|/wamp/www/page/index/future_plan.php"><i class="fa fa-caret-right"></i>Future Plan</a></li>
                <li><a href="file:///C|/wamp/www/home/contact.php"><i class="fa fa-caret-right"></i>Contact Us</a></li>
                <li><a href="file:///C|/wamp/www/page/index/admission.php"><i class="fa fa-caret-right"></i>Scholarship</a></li>
                <li><a href="file:///C|/wamp/www/page/index/study_tour.php"><i class="fa fa-caret-right"></i>Study Tour</a></li>
            </ul>
        </div>

        
        <div>
            <ul class="foot-ul-4">
                <li>Jessore University Of Science & Technology, Jessore.</li>
                <li>Phone: 091 - *****</li>
                <li>Mobile: 091 - *****</li>
                <li>Fax: 091 - *****</li>
            </ul>
        </div>
    </section>
</footer>

<section class="foot-2">
 
<div class="copyright">
	<p id="copytxt" style="margin-left:-30px !important;">&copy; Jessore University of Science & Technology</p>
</div>

	
	<div class="social">
       <!--
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
       
        <a href="#"><i class="fa fa-youtube-play"></i></a>
        <a href="#"><i class="fa fa-skype"></i></a>
        <a href="#"><i class="fa fa-yahoo"></i></a>
        -->
    </div>
    
   <div class="developer">
	<p id="developtxt">Developed by : <a href="#">Dept. of CSE</a>, JUST</p>
	</div>
</section>
	
	<!--digital clock start-->
	<script type= "text/javascript">
		$(document).ready(function(){
			var contant = $('#calendar');
			
			setInterval(function(){
				var currentdate = new Date(); 
				var datetime = "<p>" + currentdate.getDate() + "/"
				+ (currentdate.getMonth()+1)  + "/" 
				+ currentdate.getFullYear() + "</p><p> "  
				+ currentdate.getHours() + ":"  
				+ currentdate.getMinutes() + ":" 
				+ currentdate.getSeconds() + "</p>";
				
				contant.php(datetime);
			}, 1000);
		});
	</script>
	<!--digital clock end-->
</body>
</html>

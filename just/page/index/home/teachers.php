<!DOCTYPE HTML>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>JUST| Jessore</title> 

        <!----------------------------------------------************ Stylesheet ************** --------------------------------------->
        <link rel="stylesheet" type="text/css" href="../public/css/font-awesome.css"/>
        <link rel="stylesheet" type="text/css" href="../public/style.css"/>
        <link rel="stylesheet" type="text/css" href="../public/css/responsive.css"/>
        <!----------------------------------------------************ Stylesheet closed ************** --------------------------------------->

        <!----------------------------------------------************ Jquery & Plugins Started ************** -------------------------------->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"> </script>-->
        <script src="../public/js/jQuery.js"></script>

        <!----------------------------------------------************ Jquery & Plugins Closed ************** -------------------------------->

        <script src="../public/js/nav.js" type="text/javascript"></script>

        <!----------------------------------------------************ Local Script closed ************** ------------------------------------------->
        <!-- Skitter Styles -->
        <link href="../public/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />

        <!-- Skitter JS -->
        <script type="text/javascript" language="javascript" src="../public/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" language="javascript" src="../public/js/jquery.skitter.min.js"></script>

        <!-- Init Skitter -->
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $('.box_skitter_large').skitter({
                    theme: 'clean',
                    numbers_align: 'center',
                    progressbar: false,
                    dots: false,
                    preview: false
                });
            });
        </script>
    </head>

    <body>
        <div class="wrapper">
            <header class="topbar"> 
               <img src="../public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->


           				<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                                <ul class="mainmenu">
                     <li><a href="../home.html">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../page/index/at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="../page/index/just_history.php">JUST History</a> </li>
                                <li> <a href="../page/index/future_plan.php">Future Plan</a> </li>
                                <li> <a href="../page/index/just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>

                    </li>

                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                              <ul>
                                <li> <a href="../page/index/chancellor.php">Chancellor</a></li>
                                <li> <a href="../page/index/vice_chancellor.php">Vice-Chancellor</a></li>
								    <li> <a href="../page/index/university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="../page/index/administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../page/index/academic_calender.html">Academic Calender</a> </li>
                                <li> <a href="../page/index/degree_offered.php">Degree Offered</a></li>
								   <li> <a href="../page/index/admission.php">Admission</a></li>
                                <li> <a href="../page/index/academic_council.php">Academic Council</a></li>
                                <li> <a href="../page/index/academic_expenses.php">Academic Expenses</a></li>
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                           <ul>
                                <li> <a href="../page/index/bncc.php">BNCC</a> </li>
                                <li> <a href="../page/index/rover_scout.php">Rover Scout</a></li>
                                <li> <a href="../page/index/sports.php">Sports</a></li>
                                <li> <a href="../page/index/cultural_function.php">Cultural Function</a></li>
								
                            </ul>


                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="../page/index/library.php">Library</a></li>
                                <li> <a href="../page/index/accomodetion.php">Accomodetion</a></li>
                                <li> <a href="../page/index/scholarship.php">Scholarship</a></li>
                                <li> <a href="../page/index/transport.php">Transport</a></li>
								<li> <a href="../page/index/medical.php">Medical</a></li>				
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="#">Faculty Info</a>
					    <div id="soho-karzokrom">
                             <ul>
                                <li> <a href="../page/index/et.php">Engineering and Technology</a> </li>
                                <li> <a href="../page/index/bst.php">Biological Science and Technology</a></li>
								<li> <a href="../page/index/ast.php">Applied Science and Technology</a></li>
                                <li> <a href="../page/index/peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="../page/index/foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                    <li><a href="search/student.php">Students</a></li>
                    <li><a href="notice/notice.php">Notice</a></li>
                    <li><a href="overview/gallery.php">Gallery</a></li>
                    <li> <a href="contact.php">Contact</a></li>
                    <li><a href="../access/signIn.html">Login</a></li>

                </ul>


            </nav><!------------------------------------ Banner Area Started--------------------------------------->
            
          <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">


                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" onMouseOver="document.getElementById('news').stop();" onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>

            </div>
            
       <script type="text/javascript">
        $(document).ready(function() {
            
            
            
            // mouseenter + mouseout in marquee tag
            $('#news').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script>
<!--[
    - c: home/teachers
    - p: teachers
    - v: 
]-->

<article class="maincontent">
    <h1>All Teachers</h1>
    
    <section class="row">
        <figure><img src="../public/profiles/12121.jpg" /><figcaption></figcaption><figure>    </section>
</article><aside class="right-panel">
<div class="singlediv">
    <!-- <h1 class="title">Total view 0</h1>-->
        <h1 class="title"> Central Notice</h1>
        <ul>
            <li> <a href="../page/index/just_history.php"> History</a></li>
            <li> <a href="../page/index/future_plan.php">Future Plan</a></li>
            <li> <a href="../page/index/campus.php">Campus</a></li>
            <li> <a href="../page/index/academic_facilities.php">Academic Facilities</a></li>
            <li> <a href="../page/index/just_infrastructure.php"> JUST Infrastructure</a></li>
			<li> <a href="../page/index/archive.php"> Archive</a></li>

        </ul>

    </div>



 <div class="singlediv">
        <h1 class="title"> Download </h1>
        <ul>
            <li> <a href="home/overview/result.html">Result</a></li>
            <li> <a href="#">Class Routine</a></li>
            <li> <a href="#">Academic Calender</a></li>
        </ul>
    </div>
	    <div class="singlediv">
        <h1 class="title">JUST WebMail</h1>
        <ul>
            <li><a href="http://webmail.just.edu.bd:2095">
		        <p align="center"><img  src="img/webmail.jpg" width="200" height="120"></p></a>
			</li>
        </ul>
    </div>

    <div class="singlediv">
        <h1 class="title">Quick Links</h1>
        <ul>
            <li> <a href="http://www.moedu.gov.bd/" target="_blank"> MOEDU  </a></li>
            <li> <a href="http://www.dshe.gov.bd/" target="_blank">DSHE </a></li>
            <li> <a href="http://www.shikkhok.com/" target="_blank">Shikkhok.com</a></li>
            <li> <a href="http://www.bangladesh.gov.bd/" target="_blank">National Web Portal</a></li>
            <li> <a href="http://www.Jessore.gov.bd/" target="_blank">Jessore Zilla</a></li>
        </ul>

    </div>
	<div class="singlediv">
        <h1 class="title">Social Link</h1>
		<ul>
		 <li><a href="www.facebook.com">
		 <img  src="img/f.png" width="35" height="35" align= "middle"/>
		   Facebook</a></li>
		 		 <li><a href="www.twitter.com">
		 <img src="img/t.png" width="35" height="35" align= "middle" />
		   Twitter</a></li>
		 		 <li><a href="www.Feed.com">
		 <img  src="img/r.png" width="35" height="35" align= "middle" />
		  Feed</a></li>
		  		 <li><a href="www.Linkedin.com">
		 <img  src="img/l.png" width="35" height="35" align= "middle" />
		  Linkedin</a></li>
		</ul>
	</div>
	

</aside>



</div> <!------------------------------------- wrapper div closed --------------------------------------->


<footer class="foot-1">
    <section class="wrapper">
        <div>
            <ul class="foot-ul-1">
                <li><a href="search/student.php"><i class="fa fa-caret-right"></i>Students</a></li>
                <li><a href="teachers.html"><i class="fa fa-caret-right"></i>Teachers</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Committee</a></li>
				<li><a href="overview/result.php"><i class="fa fa-caret-right"></i>Result</a></li>
				<li><a href="../page/index/archive.php"><i class="fa fa-caret-right"></i>Archive</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-2">
                <li><a href="#"><i class="fa fa-caret-right"></i>Academic Calender</a></li>
                <li><a href="overview/digital_content.php"><i class="fa fa-caret-right"></i>Digital Content</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Class Routine</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Dept. Order</a></li>
                <li><a href="../page/index/admission.php"><i class="fa fa-caret-right"></i>Admission</a></li>
                <li><a href="overview/gallery.php"><i class="fa fa-caret-right"></i>Gallery</a></li>
            </ul>
        </div>

        <div class="foot-logo">
            <ul class="foot-ul-1">
                <li><a href="../page/index/bncc.php"><i class="fa fa-caret-right"></i>BNCC</a></li>
                <li><a href="../page/index/campus.php"><i class="fa fa-caret-right"></i>Campus</a></li>
                <li><a href="../page/index/rover_scout.php"><i class="fa fa-caret-right"></i>Rover Scout</a></li>
                <li><a href="../page/index/academic_facilities.php"><i class="fa fa-caret-right"></i>Academic Facilities</a></li>
                <li><a href="../page/index/just_infrastructure.php"><i class="fa fa-caret-right"></i>JUST Infrastructure</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-3">
                <li><a href="../page/index/just_achievement.php"><i class="fa fa-caret-right"></i>JUST Achievement</a></li>
                <li><a href="../page/index/just_history.php"><i class="fa fa-caret-right"></i>JUST History</a></li>
                <li><a href="../page/index/future_plan.php"><i class="fa fa-caret-right"></i>Future Plan</a></li>
                <li><a href="contact.php"><i class="fa fa-caret-right"></i>Contact Us</a></li>
                <li><a href="../page/index/admission.php"><i class="fa fa-caret-right"></i>Scholarship</a></li>
                <li><a href="../page/index/study_tour.php"><i class="fa fa-caret-right"></i>Study Tour</a></li>
            </ul>
        </div>

        
        <div>
            <ul class="foot-ul-4">
                <li>Jessore University Of Science & Technology, Jessore.</li>
                <li>Phone: 091 - *****</li>
                <li>Mobile: 091 - *****</li>
                <li>Fax: 091 - *****</li>
            </ul>
        </div>
    </section>
</footer>

<section class="foot-2">
 
<div class="copyright">
	<p id="copytxt" style="margin-left:-30px !important;">&copy; Jessore University of Science & Technology</p>
</div>

	
	<div class="social">
       <!--
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
       
        <a href="#"><i class="fa fa-youtube-play"></i></a>
        <a href="#"><i class="fa fa-skype"></i></a>
        <a href="#"><i class="fa fa-yahoo"></i></a>
        -->
    </div>
    
      <div class="developer">
	<p id="developtxt">Developed by : <a href="#">Dept. of CSE</a>, JUST</p>
	</div>
</section>
	
	<!--digital clock start-->
	<script type= "text/javascript">
		$(document).ready(function(){
			var contant = $('#calendar');
			
			setInterval(function(){
				var currentdate = new Date(); 
				var datetime = "<p>" + currentdate.getDate() + "/"
				+ (currentdate.getMonth()+1)  + "/" 
				+ currentdate.getFullYear() + "</p><p> "  
				+ currentdate.getHours() + ":"  
				+ currentdate.getMinutes() + ":" 
				+ currentdate.getSeconds() + "</p>";
				
				contant.html(datetime);
			}, 1000);
		});
	</script>
	<!--digital clock end-->
</body>
</html>

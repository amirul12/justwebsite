<!DOCTYPE HTML>
<html>
<head>
        <meta charset="utf-8">
        
        <title>JUST| Jessore</title> 
        <!----------------------------------------------************ Stylesheet ************** --------------------------------------->
        <link rel="stylesheet" type="text/css" href="public/css/font-awesome.css"/>
        <link rel="stylesheet" type="text/css" href="public/style.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/custom.css"/>
        <link rel="stylesheet" type="text/css" href="public/css/responsive.css"/>
        <!----------------------------------------------************ Stylesheet closed ************** --------------------------------------->

        <!----------------------------------------------************ Jquery & Plugins Started ************** -------------------------------->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"> </script>-->
        <script src="public/js/jQuery.js"></script>

        <!----------------------------------------------************ Jquery & Plugins Closed ************** -------------------------------->

        <script src="public/js/nav.js" type="text/javascript"></script>

        <!----------------------------------------------************ Local Script closed ************** ------------------------------------------->
        <!-- Skitter Styles -->
        <link href="public/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />

        <!-- Skitter JS -->
        <script type="text/javascript" language="javascript" src="public/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" language="javascript" src="public/js/jquery.skitter.min.js"></script>

        <!-- FFocus Slider -->


        <link rel="stylesheet" href="public/css/QFocus.css" />
        <!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
        <script src="public/js/QFocus.js"></script>

        <script>
            $(function () {
                $("#div1").QFocus();
                $("#div2").QFocus({navControl: false});
                $("#div3").QFocus({type: "fade"});
            })
        </script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>


        <!-- Init Skitter -->
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $('.box_skitter_large').skitter({
                    theme: 'clean',
                    numbers_align: 'center',
                    progressbar: false,
                    dots: false,
                    preview: false
                });
            });
        </script>
    </head>
$(document).ready(function(){
    
    // input type file value
    $('.upload').on('change', 'input[type="file"]', function(){
        var img = $(this).val();
        var ele = $(this).closest('.upload').find('label');
        $(ele).html(img);
    });
    
});
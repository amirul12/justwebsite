-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2015 at 04:07 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ourproject`
--
CREATE DATABASE IF NOT EXISTS `ourproject` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ourproject`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bnews`
--

CREATE TABLE IF NOT EXISTS `tbl_bnews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `breaking_news` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_bnews`
--

INSERT INTO `tbl_bnews` (`id`, `breaking_news`) VALUES
(1, 'mizan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cnotice_post`
--

CREATE TABLE IF NOT EXISTS `tbl_cnotice_post` (
  `cnotice_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnotice_title` varchar(255) NOT NULL,
  `cnotice_description` text NOT NULL,
  `cnotice_image` varchar(255) NOT NULL,
  `cnotice_date` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `month` varchar(255) NOT NULL,
  `cnotice_timestamp` varchar(255) NOT NULL,
  PRIMARY KEY (`cnotice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `username`, `password`) VALUES
(1, 'ourproject', '51cbd6b911b93afedb9c39a0310bc140');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE HTML>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>JUST| Jessore</title> 
 
        <!----------------------------------------------************ Stylesheet ************** --------------------------------------->
        <link rel="stylesheet" type="text/css" href="../../public/css/font-awesome.css"/>
        <link rel="stylesheet" type="text/css" href="../../public/style.css"/>
        <link rel="stylesheet" type="text/css" href="../../public/css/responsive.css"/>
        <!----------------------------------------------************ Stylesheet closed ************** --------------------------------------->

        <!----------------------------------------------************ Jquery & Plugins Started ************** -------------------------------->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"> </script>-->
        <script src="../../public/js/jQuery.js"></script>

        <!----------------------------------------------************ Jquery & Plugins Closed ************** -------------------------------->

        <script src="../../public/js/nav.js" type="text/javascript"></script>

        <!----------------------------------------------************ Local Script closed ************** ------------------------------------------->
        <!-- Skitter Styles -->
        <link href="../../public/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />

        <!-- Skitter JS -->
        <script type="text/javascript" language="javascript" src="../../public/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" language="javascript" src="../../public/js/jquery.skitter.min.js"></script>

        <!-- Init Skitter -->
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $('.box_skitter_large').skitter({
                    theme: 'clean',
                    numbers_align: 'center',
                    progressbar: false,
                    dots: false,
                    preview: false
                });
            });
        </script>
    </head>

    <body>
        <div class="wrapper">
            <header class="topbar"> 
               <img src="../../public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->


           				<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                   <ul class="mainmenu">
                    <li><a href="../../home.php">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../../page/index/at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="../../page/index/just_history.php">JUST History</a> </li>
                                <li> <a href="../../page/index/future_plan.php">Future Plan</a> </li>
                                <li> <a href="../../page/index/just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>

                    </li>

                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../../page/index/academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="../../page/index/degree_offered.php">Degree Offered</a></li>
								   <li> <a href="../../page/index/admission.php">Admission</a></li>
                                <li> <a href="../../page/index/academic_council.php">Academic Council</a></li>
                                <li> <a href="../../page/index/academic_expenses.php">Academic Expenses</a></li>
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../../page/index/bncc.php">BNCC</a> </li>
                                <li> <a href="../../page/index/rover_scout.php">Rover Scout</a></li>
                                <li> <a href="../../page/index/sports.php">Sports</a></li>
                                <li> <a href="../../page/index/cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="../../page/index/library.php">Library</a></li>
                                <li> <a href="../../page/index/accomodetion.php">Accomodetion</a></li>
                                <li> <a href="../../page/index/scholarship.php">Scholarship</a></li>
                                <li> <a href="../../page/index/transport.php">Transport</a></li>
								<li> <a href="../../page/index/medical.php">Medical</a></li>				
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="#">Faculty Info</a>
					    <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="../../page/index/et.php">Engineering and Technology</a> </li>
                                <li> <a href="../../page/index/bst.php">Biological Science and Technology</a></li>
								<li> <a href="../../page/index/ast.php">Applied Science and Technology</a></li>
                                <li> <a href="../../page/index/peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="../../page/index/foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                    <li><a href="#">Students</a></li>
                    <li><a href="../notice/notice.php">Notice</a></li>
                    <li><a href="../overview/gallery.php">Gallery</a></li>
                    <li> <a href="../contact.php">Contact</a></li>
                    <li><a href="../../access/signIn.php">Login</a></li>

                </ul>


            </nav><!------------------------------------ Banner Area Started--------------------------------------->
            
           <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
            
       
             <marquee id="news" scrollamount="3" direction="left" behavior="scroll" onMouseOver="document.getElementById('news').stop();" onMouseOut="document.getElementById('news').start();"> 
                   *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                
			</marquee>
            
            </div>
            
       <script type="text/javascript">
        $(document).ready(function() {
            
            
            
            // mouseenter + mouseout in marquee tag
            $('#news').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script><!--[
    - c: home/search
    - p: search
    - v: $page, $title, $caption
]-->

<style type="text/css">
    .result-div{
        clear: both;
        margin-top: 20px;
    }
    
    #student-result{
        width: 98%;
        border: 1px solid #72B626;
        border-radius: 3px;
        line-height: 40px;
        cursor: pointer;
        font-size: 18px;
        text-align: center;
    }
    
    #student-result:hover{
        background: #72B626;
        color: #FFF;
    }
    
    table#student-result-show{
        width: 99%;
        margin-top: 10px;
        float: left;
    }
    
    table#student-result-show tr,
    table#student-result-show tr th,
    table#student-result-show tr td{
        border-collapse: collapse;
        border: 1px solid #0088cc;
        text-align: center;
    }
</style>






<script language="javascript">
function PrintMe(DivID) {
var disp_setting="toolbar=yes,location=no,";
disp_setting+="directories=yes,menubar=yes,";
disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25";
   var content_vlue = document.getElementById(DivID).innerHTML;
   var docprint=window.open("","",disp_setting);
   docprint.document.open();
   docprint.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"');
   docprint.document.write('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
   docprint.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">');
   docprint.document.write('<head><title>My Title</title>');
   docprint.document.write('<style type="text/css">body{ margin:0px;');
   docprint.document.write('font-family:verdana,Arial;color:#000;');
   docprint.document.write('font-family:Verdana, Geneva, sans-serif; font-size:12px;}');
   
   docprint.document.write('table{width:80%;border:1px;} </style>');

   
   docprint.document.write('a{color:#000;text-decoration:none;} </style>');
   docprint.document.write('</head><body onLoad="self.print()"><center>');
   docprint.document.write(content_vlue);
   docprint.document.write('</center></body></html>');
   docprint.document.close();
   docprint.focus();
}
</script>



<article class="maincontent search">
    <h1>Search student</h1>
    
    
    <input type="button" name="btnprint" value="Print" style="float:right;" onclick="PrintMe('divid')"/>
    
    <section class="row" id="divid">
        <form method="post" action="#">
            <p>
                <label for="roll">Search by roll number </label><br/>
                <input type="text" name="roll" id="roll" />
                <input type="hidden" name="table" value="students" id="table" />
                <input type="hidden" name="session" />
            </p>
            
                    </form>
        
        <div id="result">
            <figure>
                <div id="pic"></div>
                
                <figcaption id="about">
                    <ul></ul>
                </figcaption>
            </figure>
        </div>
        
        <div class="result-div">
            <p id="student-result">See result</p>            <table id="student-result-show"></table>
        </div>
    </section>
    
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name="roll"]').on({
                'keyup': function(){
                    $('#student-result-show').php(" ");
                    
                    $.ajax({
                        'type': 'POST',
                        'url': 'http://sailerkandahighschool.edu.bd/ajax/search',
                        'data': 'roll='+$(this).val()+'&table='+$('#table').val()
                    }).done(function(result){
                        var obj = $.parseJSON(result);
                        var ele = $('#result #about ul');
                        var data = [];
                        
                        if($('#table').val() === 'teachers'){
                            // set teacher picture
                            $('#pic').php('<img src="http://sailerkandahighschool.edu.bd/public/profiles/'+ obj[0].path +'" alt="'+ obj[0].name +'" />');
                            
                            // replace underscore with space and uppercase
                            var dep = obj[0].department;
                            var depa = dep.replace('_', ' ');
                            var department = depa.substr(0, 1).toUpperCase() + depa.substr(1);
                            
                            var des = obj[0].designation;
                            var desi = des.replace('_', ' ');
                            var designation = desi.substr(0, 1).toUpperCase() + desi.substr(1);
                            
                            // set teachers descriptions                            
                            data.push('<li><span>Name</span> : ' + obj[0].name +'</li>');
                            data.push('<li><span>Department</span> : ' + department +'</li>');
                            data.push('<li><span>Designation</span> : ' + designation +'</li>');
                            data.push('<li><span>Date of birth</span> : ' + obj[0].date_of_birth +'</li>');
                            data.push('<li><span>Mobile</span> : ' + obj[0].mobile +'</li>');
                            data.push('<li><span>BCS Batch</span> : ' + obj[0].bcs_batch +'</li>');
                            data.push('<li><span>Service ID</span> : ' + obj[0].service_id +'</li>');
                            
                            if(obj[0].available === '1'){
                                data.push('<li><span>Available</span> : Yes</li>');
                            }else{
                                data.push('<li><span>Available</span> : No</li>');
                            }
                            
                            // insert data
                            ele.php( data );
                            
                            // teachers informations
                            console.log(obj[0]);
                        }else{
                            // set teacher picture
                            $('#pic').php('<img src="http://sailerkandahighschool.edu.bd/public/profiles/'+ obj[0].path +'" alt="'+ obj[0].name +'" />');
                            
                            // replace underscore with space and uppercase
                            var dep = obj[0].department;
                            var depa = dep.replace('_', ' ');
                            var department = depa.substr(0, 1).toUpperCase() + depa.substr(1);
                            
                            // set teachers descriptions
                            data.push('<li><span>Name</span> : ' + obj[0].name +'</li>');
                            data.push('<li><span>Father name</span> : ' + obj[0].father_name +'</li>');
                            data.push('<li><span>Mother name</span> : ' + obj[0].mother_name +'</li>');
                            data.push('<li><span>Department</span> : ' + department +'</li>');
                            data.push('<li><span>Roll number</span> : ' + obj[0].roll +'</li>');
                            data.push('<li><span>Session</span> : ' + obj[0].session +'</li>');
                            data.push('<li><span>Mobile</span> : ' + obj[0].mobile +'</li>');
                            data.push('<li><span>Address</span> : ' + obj[0].address +'</li>');
                               
                            $('input[name="session"]').val(obj[0].session);
                            
                            // insert data
                            ele.php( data );
                            
                            // teachers informations
                            // console.log(obj[0]);
                        }
                    });
                }
            });
            
            $('#student-result').on({
                'click': function(){
                    var rollNum = $('input[name="roll"]').val();
                    var info = {
                        roll: rollNum.trim(),
                        session: $('input[name="session"]').val()
                    };
                
                    $.ajax({
                        type: 'POST',
                        url: 'http://sailerkandahighschool.edu.bd/ajax/result',
                        data: info
                    }).done(function(result){
                        var obj = $.parseJSON(result);
                        var data = [];
                        
                        $.each(obj, function(i, e){
                            data.push('<tr><td>'+e.exam_title+'</td><td>'+e.subject_code+'</td><td>'+e.grade+'</td><td>'+e.complement+'</td></tr>');
                        });
                        
                        $('#student-result-show').php(data);
                        
                        console.log(obj);
                    });
                }
            });
            
            $('#mobile').on({
                keyup: function(){
                    var search_id = {
                        mobile: $(this).val(),
                        table: $('#table').val()
                    };
                    
                    $.ajax({
                        type: 'POST',
                        url: 'http://sailerkandahighschool.edu.bd/ajax/search_by_mobile',
                        data: search_id
                    }).done(function(result){
                        var obj = $.parseJSON(result);
                        var ele = $('#result #about ul');
                        var data = [];
                        
                        // set teacher picture
                        $('#pic').php('<img src="http://sailerkandahighschool.edu.bd/public/profiles/'+ obj[0].path +'" alt="'+ obj[0].name +'" />');

                        // replace underscore with space and uppercase
                        var dep = obj[0].department;
                        var depa = dep.replace('_', ' ');
                        var department = depa.substr(0, 1).toUpperCase() + depa.substr(1);

                        var des = obj[0].designation;
                        var desi = des.replace('_', ' ');
                        var designation = desi.substr(0, 1).toUpperCase() + desi.substr(1);

                        // set teachers descriptions                            
                        data.push('<li><span>Name</span> : ' + obj[0].name +'</li>');
                        data.push('<li><span>Department</span> : ' + department +'</li>');
                        data.push('<li><span>Designation</span> : ' + designation +'</li>');
                        data.push('<li><span>Date of birth</span> : ' + obj[0].date_of_birth +'</li>');
                        data.push('<li><span>Mobile</span> : ' + obj[0].mobile +'</li>');
                        data.push('<li><span>BCS Batch</span> : ' + obj[0].bcs_batch +'</li>');
                        data.push('<li><span>Service ID</span> : ' + obj[0].service_id +'</li>');

                        if(obj[0].available === '1'){
                            data.push('<li><span>Available</span> : Yes</li>');
                        }else{
                            data.push('<li><span>Available</span> : No</li>');
                        }

                        // insert data
                        ele.php( data );
                        // console.log(result);
                    });
                }
            });
            
        });
    </script>
</article>


<aside class="right-panel">
<div class="singlediv">
    <!-- <h1 class="title">Total view 0</h1>-->
        <h1 class="title">Central Notice</h1>
        <ul>
            <li> <a href="../../page/index/just_history.php"> History</a></li>
            <li> <a href="../../page/index/future_plan.php">Future Plan</a></li>
            <li> <a href="../../page/index/campus.php">Campus</a></li>
            <li> <a href="../../page/index/academic_facilities.php">Academic Facilities</a></li>
            <li> <a href="../../page/index/just_infrastructure.php"> JUST Infrastructure</a></li>
			<li> <a href="../../page/index/archive.php"> Archive</a></li>

        </ul>

    </div>


    <div class="singlediv">
        <h1 class="title"> Download </h1>
        <ul>
            <li> <a href="home/overview/result.php">Result</a></li>
            <li> <a href="#">Class Routine</a></li>
            <li> <a href="#">Academic Calender</a></li>
        </ul>
    </div>
	    <div class="singlediv">
        <h1 class="title">JUST WebMail</h1>
        <ul>
            <li><a href="http://webmail.just.edu.bd:2095">
		        <p align="center"><img  src="img/webmail.jpg" width="200" height="120"></p></a>
			</li>
        </ul>
    </div>



    <div class="singlediv">
        <h1 class="title">Quick Links</h1>
        <ul>
            <li> <a href="http://www.moedu.gov.bd/" target="_blank"> MOEDU  </a></li>
            <li> <a href="http://www.dshe.gov.bd/" target="_blank">DSHE </a></li>
            <li> <a href="http://www.shikkhok.com/" target="_blank">Shikkhok.com</a></li>
            <li> <a href="http://www.bangladesh.gov.bd/" target="_blank">National Web Portal</a></li>
            <li> <a href="http://www.Jessore.gov.bd/" target="_blank">Jessore Zilla</a></li>
        </ul>

    </div>
	<div class="singlediv">
        <h1 class="title">Social Link</h1>
		<ul>
		 <li><a href="www.facebook.com">
		 <img  src="img/f.png" width="35" height="35" align= "middle"/>
		   Facebook</a></li>
		 		 <li><a href="www.twitter.com">
		 <img src="img/t.png" width="35" height="35" align= "middle" />
		   Twitter</a></li>
		 		 <li><a href="www.Feed.com">
		 <img  src="img/r.png" width="35" height="35" align= "middle" />
		  Feed</a></li>
		  		 <li><a href="www.Linkedin.com">
		 <img  src="img/l.png" width="35" height="35" align= "middle" />
		  Linkedin</a></li>
		</ul>
	</div>
</aside>



</div> <!------------------------------------- wrapper div closed --------------------------------------->


<footer class="foot-1">
    <section class="wrapper">
        <div>
            <ul class="foot-ul-1">
                <li><a href="student.php"><i class="fa fa-caret-right"></i>Students</a></li>
                <li><a href="../teachers.php"><i class="fa fa-caret-right"></i>Teachers</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Committee</a></li>
				<li><a href="../overview/result.php"><i class="fa fa-caret-right"></i>Result</a></li>
				<li><a href="../../page/index/archive.php"><i class="fa fa-caret-right"></i>Archive</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-2">
                <li><a href="#"><i class="fa fa-caret-right"></i>Academic Calender</a></li>
                <li><a href="../overview/digital_content.php"><i class="fa fa-caret-right"></i>Digital Content</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Class Routine</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i>Dept. Order</a></li>
                <li><a href="../../page/index/admission.php"><i class="fa fa-caret-right"></i>Admission</a></li>
                <li><a href="../overview/gallery.php"><i class="fa fa-caret-right"></i>Gallery</a></li>
            </ul>
        </div>

        <div class="foot-logo">
            <ul class="foot-ul-1">
                <li><a href="../../page/index/bncc.php"><i class="fa fa-caret-right"></i>BNCC</a></li>
                <li><a href="../../page/index/campus.php"><i class="fa fa-caret-right"></i>Campus</a></li>
                <li><a href="../../page/index/rover_scout.php"><i class="fa fa-caret-right"></i>Rover Scout</a></li>
                <li><a href="../../page/index/academic_facilities.php"><i class="fa fa-caret-right"></i>Academic Facilities</a></li>
                <li><a href="../../page/index/just_infrastructure.php"><i class="fa fa-caret-right"></i>JUST Infrastructure</a></li>
            </ul>
        </div>

        <div>
            <ul class="foot-ul-3">
                <li><a href="../../page/index/just_achievement.php"><i class="fa fa-caret-right"></i>JUST Achievement</a></li>
                <li><a href="../../page/index/just_history.php"><i class="fa fa-caret-right"></i>JUST History</a></li>
                <li><a href="../../page/index/future_plan.php"><i class="fa fa-caret-right"></i>Future Plan</a></li>
                <li><a href="../contact.php"><i class="fa fa-caret-right"></i>Contact Us</a></li>
                <li><a href="../../page/index/admission.php"><i class="fa fa-caret-right"></i>Scholarship</a></li>
                <li><a href="../../page/index/study_tour.php"><i class="fa fa-caret-right"></i>Study Tour</a></li>
            </ul>
        </div>

        
          
        <div>
            <ul class="foot-ul-4">
                <li>Jessore University Of Science & Technology, Jessore.</li>
                <li>Phone: 091 - *****</li>
                <li>Mobile: 091 - *****</li>
                <li>Fax: 091 - *****</li>
            </ul>
        </div>
    </section>
</footer>

<section class="foot-2">
 
<div class="copyright">
	<p id="copytxt" style="margin-left:-30px !important;">&copy; Jessore University of Science & Technology</p>
</div>
	<div class="social">
       <!--
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
       
        <a href="#"><i class="fa fa-youtube-play"></i></a>
        <a href="#"><i class="fa fa-skype"></i></a>
        <a href="#"><i class="fa fa-yahoo"></i></a>
        -->
    </div>
    
  <div class="developer">
	<p id="developtxt">Developed by : <a href="#">Dept. of CSE</a>, JUST</p>
	</div>
</section>
	
	<!--digital clock start-->
	<script type= "text/javascript">
		$(document).ready(function(){
			var contant = $('#calendar');
			
			setInterval(function(){
				var currentdate = new Date(); 
				var datetime = "<p>" + currentdate.getDate() + "/"
				+ (currentdate.getMonth()+1)  + "/" 
				+ currentdate.getFullYear() + "</p><p> "  
				+ currentdate.getHours() + ":"  
				+ currentdate.getMinutes() + ":" 
				+ currentdate.getSeconds() + "</p>";
				
				contant.php(datetime);
			}, 1000);
		});
	</script>
	<!--digital clock end-->
</body>
</html>

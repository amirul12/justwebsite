<?php include("header.php") ?>
 <body>
        <div class="wrapper">
            <header class="topbar"> 
                <img src="public/img/gallery/banner.png" alt="" />
            </header> <!------------------------------------ Topbar Area Closed --------------------------------------->

		<nav class="navigation">

                <a href="#" class="responsive-icon"> <i class="fa fa-reorder fa-2x "></i></a>

                <ul class="mainmenu">
                    <li><a href="home.php">Home</a></li>
                    <li> <a href="#">About JUST</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/at_a_glance.php">JUST at a Glance</a> </li>
                                <li> <a href="page/index/just_history.php">JUST History</a> </li>
                                <li> <a href="page/index/future_plan.php">Future Plan</a> </li>
                                <li> <a href="page/index/just_achievement.php">JUST Achievement</a></li>                 
                            </ul>
                        </div>
                    </li>
                    <li> <a href="#">Administration</a> 
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/chancellor.php">Chancellor</a></li>
                                <li> <a href="page/index/vice_chancellor.php">Vice-Chancellor</a></li>
								<li> <a href="page/index/university_syndicate.php">University Syndicate</a></li>
                                <li> <a href="page/index/administrative_office.php">Administrative Office</a></li>
                            </ul>
                        </div>

                    </li>
                    <li> <a href="#">Academic</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/academic_calender.php">Academic Calender</a> </li>
                                <li> <a href="page/index/degree_offered.php">Degree Offered</a></li>
								<li> <a href="page/index/admission.php">Admission</a></li>
                                <li> <a href="page/index/academic_council.php">Academic Council</a></li>
                                <li> <a href="page/index/academic_expenses.php">Academic Expenses</a></li>                     
                            </ul>

                        </div>
                    </li>

                    <li> <a href="#">Co-Curiculums</a>
                        <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/bncc.php">BNCC</a> </li>
                                <li> <a href="page/index/rover_scout.php">Rover Scout</a></li>
                                <li> <a href="page/index/sports.php">Sports</a></li>
                                <li> <a href="page/index/cultural_function.php">Cultural Function</a></li>
								
                            </ul>

                        </div>
                    </li>


                    <li> <a href="#">Facilities</a>
                        <div id="soho-karzokrom">
                            <ul>
								<li> <a href="page/index/library.php">Library</a></li>
                                <li> <a href="page/index/academic_facilities.php">Accomodetion</a></li>
                                <li> <a href="page/index/scholarship.php">Scholarship</a></li>
                                <li> <a href="page/index/study_tour.php">Transport</a></li>
								<li> <a href="page/index/medical.php">Medical</a></li>					
                            </ul>
                        </div>
                    </li>			

                  
                    <li><a href="#">Faculty Info</a>
					    <div id="soho-karzokrom">
                            <ul>
                                <li> <a href="page/index/et.php">Engineering and Technology</a> </li>
                                <li> <a href="page/index/bst.php">Biological Science and Technology</a></li>
								<li> <a href="page/index/ast.php">Applied Science and Technology</a></li>
                                <li> <a href="page/index/peles.php">Physical education,Language and Ethical Studies</a></li>
                                <li> <a href="page/index/foc.php">Faculty of Science</a></li>                     
                            </ul>

                        </div>
					
					</li>
                    <li><a href="home/search/student.php">Students</a></li>
                    <li><a href="home/notice/notice.php">Notice</a></li>
                    <li><a href="home/overview/gallery.php">Gallery</a></li>
                    <li> <a href="home/contact.php">Contact</a></li>
                    <li><a href="access/signIn.html">Login</a></li>

                </ul>
            </nav>
 
 <!------------------------------------ Banner Area Closed --------------------------------------->

            <div class="marquee" style="padding:10px 10px;color:#0164FA;font-weight:bold;border:1px solid #0164FA;margin-top:0px;">
                <marquee id="news" scrollamount="3" direction="left" behavior="scroll" 
				onMouseOver="document.getElementById('news').stop();" 
				onMouseOut="document.getElementById('news').start();"> 
                    *** <a style="color:#0164FA;" href="#">Jessore University of Science & Technology</a>                 
				</marquee>
            </div>

            <div class="banner-main">
                <div class="banner1">
                    <div id="div1" class="QFocus">
                        <ul>
                            <li><a href='#'><img src='public/slider/1.jpg' title='AcademyBuilding' /></a></li>
							<li><a href='#'><img src='public/slider/2.jpg' title='Dormetory' /></a></li> 
							<li><a href='#'><img src='public/slider/3.jpg' title='AdministrativeBuilding' /></a></li>
							<li><a href='#'><img src='public/slider/4.jpg' title='Womens Hall' /></a></li> 							
						</ul>
                    </div>
                </div>
                
                <div class="banner2">
                    <div class="noticeboard-top social">
                        <h3>Departmental Notice</h3>
                        <marquee id="d_news" behavior="scroll" direction="up" scrollamount="3" onMouseOver="document.getElementById('d_news').stop();" 
							onMouseOut="document.getElementById('d_news').start();">
                            <p><a href="#">Dept. Of CSE</a></p> 
							<p><a href="#">Dept. Of EEE</a></p> 
							<p><a href="#">Dept. Of PME</a></p> 
							<p><a href="#">Dept. Of IPE</a></p> 
							<p><a href="#">Dept. Of ChE</a></p> 
							<p><a href="#">Dept. Of MB</a></p> 
							<p><a href="#">Dept. Of FMB</a></p>							

						</marquee>
                    </div>
                    
                </div>
            </div>
            <!------------------------------------ Banner Area Closed --------------------------------------->

          <script type="text/javascript">
                $(document).ready(function () {
                    // mouseenter + mouseout in marquee tag
                    $('#news').on({
                        'mouseenter': function () {
                            $(this).closest('marquee').attr('scrollamount', 0);
                        },
                        'mouseout': function () {
                            $(this).closest('marquee').attr('scrollamount', 3);
                        }
                    });
                });
            </script>
			 <script type="text/javascript">
                $(document).ready(function () {
                    // mouseenter + mouseout in marquee tag
                    $('#d_news').on({
                        'mouseenter': function () {
                            $(this).closest('marquee').attr('scrollamount', 0);
                        },
                        'mouseout': function () {
                            $(this).closest('marquee').attr('scrollamount', 3);
                        }
                    });
                });
            </script>
            
            <script type="text/javascript">
        $(document).ready(function() {
            // mouseenter + mouseout in marquee tag
            $('.noticeboard-top marquee p a').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script>
<style>
.maincontent{}
</style>

<!--[
    - c: home/index
    - p: home
    - v: $title, $admin, $honour, $master, $hsc, $view
]-->

<article class="maincontent" style="margin-top:0px;">  
   
       <section class="sammary row">
   
		<h2 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc; color:#0164FA;">Welcome to JUST</h2>
		<p style="padding: 10px; text-align: justify; line-height: 25px;font-size:15px;">    
	    Jessore University of Science and Technology (JUST) is a newly established renowned public university of Bangladesh. It was established in 2007. In 2010, Hon'ble Prime Minister Sheikh Hasina formally inaugurated the new campus of JUST.
	</section>
	
	<section class="sammary row">
       <img src="public/img/vc.jpg"alt="" width="110" style="margin:3px 10px 5px 3px;float:left;"/>
        <h3 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc;color:#0164FA;">Message from Vice Chancellor..</h3>
	<p style="padding: 10px; text-align: justify; line-height: 25px;font-size:15px;">
							It is my great pleasure to invite you to explore the Jessore University of Science and
							Technology (JUST) online through our website. Contribution of science and technology for 
							developing a nation is well known to all. To meet up the diversified demand of people, 
							information and communication technology,and biological sciences are playing the key role. 
							University is the most suitable place for education and research. Universities are playing a vital
							role in building efficient manpower for the development of the country as well as for the global 
							need. With a view to imparting science and technology oriented education in Bangladesh, the Jessore
							University of Science & Technology was established in 2007 by the Shadhinota Shorok (Independence 
							Road) in Jessore district. The first batch of students was admitted in 2008-2009 session. The 
							honorable Prime Minister of the Peoples Republic of Bangladesh, Sheikh Hasina, inaugurated the 
							campus of the university on December 27, 2010.Within a very short period, the JUST already 
							demonstrated its outreach excellence through establishing linkages with the various government 
							and non-government organizations for research, extension and developmental activities. Our 
							honorable Prime Minister has already declared the Vision 2021 for the digitization (advance 
							technology based, economically developed, corruption free) of the country, Bangladesh. I strongly
							believe that this university will play a significant role to fulfil this vision by producing 
							science and technology based efficient manpower and enlightened citizen. We always adapt the 
							positive changes which is the need of time. Thus, by using this science and technological 
							knowledge we are motivated to boost up the world's technology. As the Vice Chancellor of the 
							Jessore University of Science and Technology, I congratulate all and wish its success in future.
	</section>
    <section class="sammary row">
         <h2 style="font-weight:bold;padding:5px;border-bottom:1px solid #ccc;color:#0164FA;">News and Events</h2>			
			<div class="newsandevents social">
			  <ul>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Dept. Of Computer Science & Engneering</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Chairman of NHRC Visits JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Intel Engineer Visits JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>National Mourning Day Observed at JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>National Fisheries Week Observed at JUST.</a></li>
			    <li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Teachers Abstain from Work.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Japanese Delegate Visited JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Seismic Station Installed at JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>Prof. Dr. S. Bhowmick, University of Kalyani joined at JUST.</a></li>
				<li><a href="#">
		        <img  src="home/img/hand.jpg" width="30" height="26" align= "middle"/>JUST Achieved National Award on Tree Plantation.</a></li>
			  </ul>
			</div>
    </section>
    

    <script type="text/javascript">
        $(document).ready(function() {
            // mouseenter + mouseout in marquee tag
            $('.noticeboard marquee p a').on({
                'mouseenter': function() {
                    $(this).closest('marquee').attr('scrollamount', 0);
                },
                'mouseout': function() {
                    $(this).closest('marquee').attr('scrollamount', 3);
                }
            });
        });
    </script>
</article>


<?php include("rightside.php") ?>



</div> <!------------------------------------- wrapper div closed --------------------------------------->

<?php include("footer.php") ?>
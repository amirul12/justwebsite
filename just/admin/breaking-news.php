<?php
ob_start();
session_start();
if($_SESSION['name']!='admin')
{
	header('location: login.php');
}
include("../config.php");
?>

<?php
if(isset($_POST['form1'])) {
	
	try {
	
		if(empty($_POST['breaking_news'])) {
			throw new Exception("Footer Text can not be empty");
		}
		
		$statement = $db->prepare("UPDATE tbl_bnews SET breaking_news=? WHERE id=1");
		$statement->execute(array($_POST['breaking_news']));
		
		$success_message = "updated successfully.";
		
	
	}
	
	catch(Exception $e) {
		$error_message = $e->getMessage();
	}
	
	
}
?>


<?php
$statement = $db->prepare("SELECT * FROM tbl_bnews WHERE id=1");
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach($result as $row)
{
	$breaking_news = $row['breaking_news'];
}


?>
<?php include("header.php"); ?>
<h2>Update breaking news text</h2>

<?php
if(isset($error_message)) {echo "<div class='error'>".$error_message."</div>";}
if(isset($success_message)) {echo "<div class='success'>".$success_message."</div>";}
?>

<form action="" method="post">
<table class="tbl1">
<tr>
	<td>Breaking News Text</td>
</tr>
<tr>
	<td><input class="long" type="text" name="breaking_news" value="<?php echo $breaking_news; ?>"></td>
</tr>
<tr>
	<td><input type="submit" value="UPDATE" name="form1"></td>
</tr>
</table>	
</form>

<?php include("footer.php"); ?>			
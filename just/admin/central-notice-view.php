<?php
ob_start();
session_start();
if($_SESSION['name']!='admin')
{
	header('location: login.php');
}
include("../config.php");
?>
<?php include("header.php"); ?>
<h2>View  All Posts</h2>

<table class="tbl2" width="100%">
	<tr>
		<th width="5%">Serial</th>
		<th width="65%">Title</th>
		<th width="30%">Action</th>
	</tr>
	
	<?php
	$i=0;
	$statement = $db->prepare("SELECT * FROM tbl_cnotice_post ORDER BY cnotice_id DESC");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row)
	{
		$i++;
		?>
		
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $row['cnotice_title']; ?></td>
		<td>
			<a class="fancybox" href="#inline<?php echo $i; ?>">View</a>
			<div id="inline<?php echo $i; ?>" style="width:700px;display: none;">
				<h3 style="border-bottom:2px solid #808080;margin-bottom:10px;">View All Data</h3>
				<p>
					<form action="" method="post">
					<table>
						<tr>
							<td><b>Title</b></td>
						</tr>
						<tr>
							<td><?php echo $row['cnotice_title']; ?></td>
						</tr>
						<tr>
							<td><b>Description</b></td>
						</tr>
						<tr>
							<td><?php echo $row['cnotice_description']; ?></td>
						</tr>
						<tr>
							<td><b>Image</b></td>
						</tr>
						<tr>
							<td><img src="../uploads/<?php echo $row['cnotice_image']; ?>" alt=""></td>
						</tr>				
						
					</table>
					</form>
				</p>
			</div>
			&nbsp;|&nbsp;
			<a href="central-notice-edit.php?id=<?php echo $row['cnotice_id'];?>">Edit</a>
			&nbsp;|&nbsp;
			<a onclick='return confirmDelete();' href="central-notice-delete.php?id=<?php echo $row['cnotice_id'];?>">Delete</a></td>
	</tr>


		<?php
	}
	?>
	
	
	
	
</table>


<?php include("footer.php"); ?>			
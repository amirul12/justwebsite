<?php
ob_start();
session_start();
if($_SESSION['name']!='admin')
{
	header('location: login.php');
}
include("../config.php");
?>

<?php

if(isset($_POST['form1'])) {

	try {
	
		if(empty($_POST['cnotice_title'])) {
			throw new Exception("Title can not be empty.");
		}
		
		if(empty($_POST['cnotice_description'])) {
			throw new Exception("Description can not be empty.");
		}		

		$statement = $db->prepare("SHOW TABLE STATUS LIKE 'tbl_cnotice_post'");
		$statement->execute();
		$result = $statement->fetchAll();
		foreach($result as $row)
			$new_id = $row[10];
			
		$up_filename=$_FILES["cnotice_image"]["name"];
		$file_basename = substr($up_filename, 0, strripos($up_filename, '.')); // strip extention
		$file_ext = substr($up_filename, strripos($up_filename, '.')); // strip name
		$f1 = $new_id . $file_ext;
		
		if(($file_ext!='.png')&&($file_ext!='.jpg')&&($file_ext!='.jpeg')&&($file_ext!='.gif'))
			throw new Exception("Only jpg, jpeg, png and gif format images are allowed to upload.");
		
		move_uploaded_file($_FILES["cnotice_image"]["tmp_name"],"../uploads/" . $f1);
		
		$post_date = date('Y-m-d');
		$post_timestamp = strtotime(date('Y-m-d'));
		$year = substr($post_date,0,4);
		$month = substr($post_date,5,2);
		
		$statement = $db->prepare("INSERT INTO tbl_cnotice_post (cnotice_title,cnotice_description,cnotice_image,cnotice_date,year,month,cnotice_timestamp) VALUES (?,?,?,?,?,?,?)");
		$statement->execute(array($_POST['cnotice_title'],$_POST['cnotice_description'],$f1,$post_date,$year,$month,$post_timestamp));
		
		
		$success_message = "Post is inserted successfully.";
	}
	
	catch(Exception $e) {
		$error_message = $e->getMessage();
	}

}

?>

<?php include("header.php"); ?>
<h2>Add New Post</h2>

<?php
if(isset($error_message)) {echo "<div class='error'>".$error_message."</div>";}
if(isset($success_message)) {echo "<div class='success'>".$success_message."</div>";}
?>

<form action="" method="post" enctype="multipart/form-data">
<table class="tbl1">
<tr><td>Title</td></tr>
<tr><td><input class="long" type="text" name="cnotice_title"></td></tr>
<tr><td>Description</td></tr>
<tr>
<td>
<textarea name="cnotice_description" cols="30" rows="10"></textarea>
<script type="text/javascript">
	if ( typeof CKEDITOR == 'undefined' )
	{
		document.write(
			'<strong><span style="color: #ff0000">Error</span>: CKEditor not found</strong>.' +
			'This sample assumes that CKEditor (not included with CKFinder) is installed in' +
			'the "/ckeditor/" path. If you have it installed in a different place, just edit' +
			'this file, changing the wrong paths in the &lt;head&gt; (line 5) and the "BasePath"' +
			'value (line 32).' ) ;
	}
	else
	{
		var editor = CKEDITOR.replace( 'cnotice_description' );
		//editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
	}

</script>
</td>
</tr>
<tr><td>Featured Image</td></tr>
<tr><td><input type="file" name="cnotice_image"></td></tr>

<tr><td><input type="submit" value="SAVE" name="form1"></td></tr>
</table>	
</form>
<?php include("footer.php"); ?>			
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dashboard Page</title>
	<link rel="stylesheet" href="../style-admin.css">
	
	<script type='text/javascript'>
	function confirmDelete()
	{
		return confirm("Do you sure want to delete this data?");
	}
	</script>
	<!-- Fancybox jQuery -->
	<script type="text/javascript" src="../fancybox/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="../fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript" src="../fancybox/main.js"></script>
	<link rel="stylesheet" type="text/css" href="../fancybox/jquery.fancybox.css" />
	<!-- //Fancybox jQuery -->
	
	<!-- CKEditor Start -->
	<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
	<!-- // CKEditor End -->
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<h1> Admin Panel Dashboard</h1>
		</div>
		<div id="container">
			<div id="sidebar">
				<h2> Control Options</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="breaking-news.php">Breaking News Manage</a></li>
					<li><a href="central-notice.php">Central Notice Manage</a></li>
					<li><a href="">News and Events Manage</a></li>
					<li><a href="">Photo galary Manage</a></li>
					<li><a href="logout.php">Logout</a></li>
				</ul>
			</div>
			<div id="content">
<?php
ob_start();
session_start();
if($_SESSION['name']!='admin')
{
	header('location: login.php');
}
include("../config.php");
?>

<?php

if(!isset($_REQUEST['id'])) {
	header("location: central-notice-view.php");
}
else {
	$id = $_REQUEST['id'];
}
?>


<?php

if(isset($_POST['form1'])) {


	try {
	
		if(empty($_POST['cnotice_title'])) {
			throw new Exception("Title can not be empty.");
		}
		
		if(empty($_POST['cnotice_description'])) {
			throw new Exception("Description can not be empty.");
		}		
		
		if(empty($_FILES["cnotice_image"]["name"])) {
						
			$statement = $db->prepare("UPDATE tbl_cnotice_post SET cnotice_title=?, cnotice_description=? WHERE cnotice_id=?");
			$statement->execute(array($_POST['cnotice_title'],$_POST['cnotice_description'],$id));
			
		}
		else {
			
			$up_filename=$_FILES["cnotice_image"]["name"];
			$file_basename = substr($up_filename, 0, strripos($up_filename, '.')); 
			$file_ext = substr($up_filename, strripos($up_filename, '.'));
			$f1 = $id . $file_ext;
		
			if(($file_ext!='.png')&&($file_ext!='.jpg')&&($file_ext!='.jpeg')&&($file_ext!='.gif'))
				throw new Exception("Only jpg, jpeg, png and gif format images are allowed to upload.");
			
			
			$statement = $db->prepare("SELECT * FROM tbl_cnotice_post WHERE cnotice_id=?");
			$statement->execute(array($id));
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			foreach($result as $row)
			{
				$real_path = "../uploads/".$row['cnotice_image'];
				unlink($real_path);
			}
			move_uploaded_file($_FILES["cnotice_image"]["tmp_name"],"../uploads/" . $f1);
			
			
			$statement = $db->prepare("UPDATE tbl_cnotice_post SET cnotice_title=?, cnotice_description=?,cnotice_image=? WHERE cnotice_id=?");
			$statement->execute(array($_POST['cnotice_title'],$_POST['cnotice_description'],$f1,$id));
			
		}
		
		$success_message = "Post is updated successfully.";
		
		
	
	}
	
	catch(Exception $e) {
		$error_message = $e->getMessage();
	}


}

?>

<?php
$statement = $db->prepare("SELECT * FROM tbl_cnotice_post WHERE cnotice_id=?");
$statement->execute(array($id));
$result = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach($result as $row)
{
	$cnotice_title = $row['cnotice_title'];
	$cnotice_description = $row['cnotice_description'];
	$cnotice_image = $row['cnotice_image'];
}
?>

<?php include("header.php"); ?>
<h2>Edit Post</h2>

<?php
if(isset($error_message)) {echo "<div class='error'>".$error_message."</div>";}
if(isset($success_message)) {echo "<div class='success'>".$success_message."</div>";}
?>

<form action="central-notice-edit.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
<!--<input type="hidden" name="id" value="<?php echo $id; ?>">-->
<table class="tbl1">
<tr><td>Title</td></tr>
<tr><td><input class="long" type="text" name="cnotice_title" value="<?php echo $cnotice_title; ?>"></td></tr>
<tr><td>Description</td></tr>
<tr>
<td>
<textarea name="cnotice_description" cols="30" rows="10"><?php echo $cnotice_description; ?></textarea>
<script type="text/javascript">
	if ( typeof CKEDITOR == 'undefined' )
	{
		document.write(
			'<strong><span style="color: #ff0000">Error</span>: CKEditor not found</strong>.' +
			'This sample assumes that CKEditor (not included with CKFinder) is installed in' +
			'the "/ckeditor/" path. If you have it installed in a different place, just edit' +
			'this file, changing the wrong paths in the &lt;head&gt; (line 5) and the "BasePath"' +
			'value (line 32).' ) ;
	}
	else
	{
		var editor = CKEDITOR.replace( 'cnotice_description' );
		//editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
	}

</script>
</td>
</tr>
<tr><td>Previous Featured Image Preview</td></tr>
<tr><td><img src="../uploads/<?php echo $cnotice_image; ?>" alt="" width="200"></td></tr>
<tr><td>New Featured Image</td></tr>
<tr><td><input type="file" name="cnotice_image"></td></tr>

<tr><td><input type="submit" value="UPDATE" name="form1"></td></tr>
</table>	
</form>
<?php include("footer.php"); ?>			